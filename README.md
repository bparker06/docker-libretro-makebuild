Test docker container to build simple Makefile-based libretro cores. Does not use libretro-super.

After checking out repo:

To build:

docker build -t libretro-makebuild .

To run (using mame as an example core):

docker run -it -e CORE=mame -e PTR64=1 -e MAKEFILE_NAME=Makefile.libretro libretro-makebuild

Check the `make.sh` file for other environment variables that can be set to customize the build process.
