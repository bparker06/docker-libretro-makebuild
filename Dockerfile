FROM ubuntu:18.04

ENV PYTHONUNBUFFERED 1
ENV container docker
ENV LC_ALL C
ENV DEBIAN_FRONTEND noninteractive

#####

RUN sed -i 's/# deb/deb/g' /etc/apt/sources.list

RUN useradd --no-log-init -m -r -g root -d /home/docker docker && apt-get update \
    && apt-get -y dist-upgrade \
    && apt-get -y install build-essential g++-8 git python libgl-dev isc-dhcp-client iproute2 awscli \
    && update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 100 --slave /usr/bin/g++ g++ /usr/bin/g++-8

USER docker

WORKDIR /home/docker

COPY . .

CMD ["./make.sh"]
