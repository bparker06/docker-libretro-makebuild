#!/bin/bash -x

PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin

platform=${platform:="unix"}
PLATFORM=$platform
# don't use simply MAKEFILE!!! mame will break
MAKEFILE_NAME=${MAKEFILE_NAME:="Makefile"}
GIT_DEPTH=${GIT_DEPTH:=1}
GIT_BRANCH=${GIT_BRANCH:="master"}
JOBS=${JOBS:=$(nproc)}
MAKE_DIR=${MAKE_DIR:=.}

uname -a

cat /etc/os-release

if [ "${DHCP}" = "1" ]; then
  dhclient -v
fi

ip a
ip r

if [ -z "${GIT_URL}" ]; then
  if [ -z "${CORE}" ]; then
    echo "No git URL was specified and no core was specified. Nothing to do."
    exit 1
  else
    GIT_URL="https://github.com/libretro/${CORE}"
  fi
fi

echo "Using git url: ${GIT_URL}"

rm -fr work

if [ -n "${CLONE_CMD}" ]; then
  echo "Running clone command..."
  eval ${CLONE_CMD}
else
  echo "Running git clone..."
  git clone --depth=${GIT_DEPTH} -b ${GIT_BRANCH} ${GIT_URL} work
fi

cd work

if [ -n "${CUSTOM_CMD}" ]; then
  echo "Running custom command..."
  ${CUSTOM_CMD} $@
else
  echo "Running make command..."
  make -j${JOBS} -C ${MAKE_DIR} -f ${MAKEFILE_NAME} $@
fi

if [ -n "${DEPLOY_CMD}" ]; then
  echo "Running deploy command..."
  eval ${DEPLOY_CMD}
fi
